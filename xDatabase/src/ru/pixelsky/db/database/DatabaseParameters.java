package ru.pixelsky.db.database;

public final class DatabaseParameters {
    private final String url;
    private final String username;
    private final String password;
    private final String driverClass;

    public DatabaseParameters(String url, String username, String password, String driverClass) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.driverClass = driverClass;
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDriverClass() {
        return driverClass;
    }
}
