package ru.pixelsky.db.database;

import org.apache.commons.lang.Validate;
import ru.pixelsky.db.exceptions.DatabaseConnectionException;
import ru.pixelsky.db.exceptions.DatabaseException;
import ru.pixelsky.db.exceptions.DatabaseSQLException;
import ru.pixelsky.db.query.QueryResult;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;

public class DatabaseStatement {
    private Database database;
    private String sql;

    public DatabaseStatement(Database database, String sql) {
        Validate.notNull(database);
        Validate.notNull(sql);
        this.database = database;
        this.sql = sql;
    }

    public QueryResult executeQuery(Object... params) throws DatabaseException {
        Connection connection = database.getConnection();
        if (connection == null) {
            throw new DatabaseConnectionException("No connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            return QueryResult.fromResultSet(statement.executeQuery());
        } catch (SQLException e) {
            throw new DatabaseSQLException(e);
        } finally {
            closeConnection(connection);
        }
    }

    public int executeUpdate(Object... params) throws DatabaseException {
        Connection connection = database.getConnection();
        if (connection == null) {
            throw new DatabaseConnectionException("No connection");
        }
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseSQLException(e);
        } finally {
            closeConnection(connection);
        }
    }

    private void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            database.getLogger().log(Level.SEVERE, "Error closing connection", e);
        }
    }
}
