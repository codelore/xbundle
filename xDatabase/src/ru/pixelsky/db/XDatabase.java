package ru.pixelsky.db;

import org.bukkit.configuration.Configuration;
import org.bukkit.plugin.java.JavaPlugin;
import ru.pixelsky.db.database.Database;
import ru.pixelsky.db.database.DatabaseParameters;
import ru.pixelsky.db.exceptions.DatabaseException;

import java.util.logging.Level;
import java.util.logging.Logger;

public class XDatabase extends JavaPlugin {
    static Logger logger;

    @Override
    public void onDisable() {
        try {
            Databases.getDefault().shutdown();
        } catch (DatabaseException e) {
            logger.log(Level.SEVERE, "Error shutting down database");
        }
    }

    public void onEnable() {
        logger = getLogger();
        getCommand("db").setExecutor(new CommandHandler());
        saveDefaultConfig();
        Configuration config = getConfig();
        String url = config.getString("url");
        String username = config.getString("username");
        String password = config.getString("password");
        String driver = config.getString("driver");

        Database database = new Database(logger, new DatabaseParameters(url, username, password, driver));
        Databases.setDefaultDatabase(database);
        if(database.testConnection())
            logger.log(Level.INFO, "Connection OK");
        else
            logger.log(Level.INFO, "Connection fail =(");
    }
}
