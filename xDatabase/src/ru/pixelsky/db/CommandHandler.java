package ru.pixelsky.db;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandHandler implements CommandExecutor {
    @Override
    public boolean onCommand(final CommandSender sender, Command command, String label, String[] args) {
        Thread thread = new Thread() {
            public void run() {
                boolean dbTestResult = Databases.getDefault().testConnection();
                sender.sendMessage(dbTestResult ?
                        ChatColor.GREEN + "Соединение с БД работает нормально" :
                        ChatColor.RED + "Проблемы с подсоединением к БД");
            }
        };
        thread.start();
        return true;
    }
}
