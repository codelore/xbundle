package ru.pixelsky.db.query;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.Arrays;

public class LargeData {
    private byte[] data;

    private LargeData(byte[] data) {
        this.data = data;
    }

    public static LargeData valueOf(Blob blob) throws SQLException {
        byte[] data = blob.getBytes(0, (int) blob.length());
        return new LargeData(data);
    }

    public static LargeData valueOf(byte[] data) {
        return new LargeData(Arrays.copyOf(data, data.length));
    }

    public byte[] getData() {
        return data;
    }

    public byte[] copyData() {
        return Arrays.copyOf(data, data.length);
    }
}
