package net.oxmc.xcustomname;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRegisterChannelEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.pixelsky.db.exceptions.DatabaseException;

import java.io.IOException;
import java.util.logging.Level;

public class XCustomName extends JavaPlugin
{
    static XCustomName plugin;
    DisplayNameManager displayNameManager;
    DisplayNamesStorage storage;
    XCNCommandHandler handler = new XCNCommandHandler();

    public XCustomName() {
        plugin = this;
    }

    public void onEnable()
    {
        this.displayNameManager = new DisplayNameManager(this);
        displayNameManager.init();

        try {
            // Подключение к БД
            this.storage = new DisplayNamesStorage(getLogger());
            this.storage.init();
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, "Error while loading SQL", e);
        } catch (DatabaseException e) {
            getLogger().log(Level.SEVERE, "Error while creating table", e);
        }

        // Command handlers
        getCommand("name").setExecutor(handler);
        getCommand("setname").setExecutor(handler);

        getServer().getPluginManager().registerEvents(new Listener()
        {
            @EventHandler
            public void onJoin(PlayerRegisterChannelEvent event) {
                Player player = event.getPlayer();
                try {
                    // Получение отображаемого ника из БД
                    String displayName = storage.loadDisplayName(player.getName());
                    if (displayName != null)
                        displayNameManager.setDisplayName(player.getName(), displayName);
                } catch (DatabaseException e) {
                    getLogger().log(Level.SEVERE, "Error loading player's display name on join", e);
                }

                // Передача информации о нике
                displayNameManager.sendAllDisplayNames(player);
                displayNameManager.sendUpdatedName(player.getName());
            }
        }, plugin);
    }
}