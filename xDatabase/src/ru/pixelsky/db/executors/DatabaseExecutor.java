package ru.pixelsky.db.executors;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseExecutor {
    private static final Logger log = Logger.getLogger(DatabaseExecutor.class.getName());
    private static final ExecutorService taskExecutor = Executors.newFixedThreadPool(4);

    public <T> void execute(final Plugin plugin, final Callable<? extends T> databaseRunnable, final BukkitRunnable<T> callback) {
        taskExecutor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    final T obj = databaseRunnable.call();
                    Bukkit.getScheduler().runTask(plugin, new Runnable() {
                        @Override
                        public void run() {
                            callback.run(obj);
                        }
                    });
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Error executing a task", e);
                }
            }
        });
    }
}
